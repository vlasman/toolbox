ARG KANIKO_IMAGE_NAME
FROM ${KANIKO_IMAGE_NAME} AS kaniko-executor

FROM docker.io/library/alpine:3.21.3

ENV DOCKER_CONFIG="/kaniko/" \
    KANIKO_CONFIG_FILE="/kaniko/config.json"

RUN apk add --no-cache \
      apache2-utils \
      colordiff \
      curl \
      file \
      git \
      go \
      libffi \
      make \
      moreutils \
      npm \
      openssl \
      shellcheck \
      skopeo \
 && npm install --global prettier@3.3.1

RUN wget -O /dev/stdout https://get.helm.sh/helm-v3.14.0-linux-amd64.tar.gz | tar -xz -C /usr/bin --strip-components=1 linux-amd64/helm \
 && sha256sum /usr/bin/helm \
 && echo "fbd63ae4cef1fa836d1052cb977593d4d5ce6e5efdec8aa912452308b2302bde  /usr/bin/helm" | sha256sum -c \
 && wget -O /usr/bin/kubectl https://dl.k8s.io/v1.30.1/bin/linux/amd64/kubectl \
 && sha256sum /usr/bin/kubectl \
 && echo "5b86f0b06e1a5ba6f8f00e2b01e8ed39407729c4990aeda961f83a586f975e8a  /usr/bin/kubectl" | sha256sum -c \
 && chmod +x /usr/bin/kubectl \
 && wget -O /usr/bin/jq https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 \
 && sha256sum /usr/bin/jq \
 && echo "af986793a515d500ab2d35f8d2aecd656e764504b789b66d7e1a0b727a124c44  /usr/bin/jq" | sha256sum -c \
 && chmod +x /usr/bin/jq \
 && wget -O /usr/bin/yq https://github.com/mikefarah/yq/releases/download/v4.44.1/yq_linux_amd64 \
 && sha256sum /usr/bin/yq \
 && echo "6dc2d0cd4e0caca5aeffd0d784a48263591080e4a0895abe69f3a76eb50d1ba3  /usr/bin/yq" | sha256sum -c \
 && chmod +x /usr/bin/yq \
 && wget -O /usr/bin/devspace https://github.com/devspace-sh/devspace/releases/download/v6.3.12/devspace-linux-amd64 \
 && sha256sum /usr/bin/devspace \
 && echo "163b4159cb0ef1c832c74b8da5ed6adb834ccffc87e57cb7fc8ffc4fb4912c62  /usr/bin/devspace" | sha256sum -c \
 && chmod +x /usr/bin/devspace \
 && wget -O /dev/stdout https://github.com/brancz/gojsontoyaml/releases/download/v0.1.0/gojsontoyaml_0.1.0_linux_amd64.tar.gz | tar -xz -C /usr/bin \
 && sha256sum /usr/bin/gojsontoyaml \
 && echo "e8cea6e31b32457b3c3fd5af198a6602a546eb480649f2833e0e64f877de04c4  /usr/bin/gojsontoyaml" | sha256sum -c \
 && chmod +x /usr/bin/gojsontoyaml \
 && echo '{"test": "hello world"}' | gojsontoyaml \
 && echo 'test: "hello world"' | gojsontoyaml -yamltojson \
 && rm -rf \
      ${GOPATH} \
      /tmp/*

COPY --from=kaniko-executor /kaniko/executor /kaniko/warmer /kaniko/
